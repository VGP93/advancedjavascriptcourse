'use strict'

function Product() {
    let price = 5;
    let quantity = 500;

    this.brand = "chair";

    this.show = function () {
        console.log(`${price}  ${quantity} ${this.brand} ${calculateProfit()}`);
    }
    
    function calculateProfit(){
        return quantity * price;
    }

}

let product = new Product();
product.show();