'use strict';

const Product = (function() {
    let price;
    let quantity;

    function display() {
        console.log(`${price} ${quantity} ${calculateProfit()}`);
    }
    function calculateProfit() {
       return price * quantity;
    }
    function register(prc,qnt) {
        price = prc;
        quantity = qnt;
    }

    return {
        print: display,
        add: register,
        color: "black",

    
    }

})();

Product.add('5','5000');
Product.print();