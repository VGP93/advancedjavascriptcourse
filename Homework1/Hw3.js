function Product(){

    this.show = function(){
        console.log(`${this.price} ${this.quantity}`)
    }
    
}

Object.defineProperties( Product.prototype, {
    price: {
        set: function(val){
            console.log('Value');
            this.__val = val;
        },
        get: function() {
            console.log('Get Value');
            return this.__val;
        }
    },
    quantity: {
        set: function(qnt) {
            console.log('Quantity');
            this.__qnt = qnt;
        },
        get: function(){
            console.log('Get Quantity');
            return this.__qnt;
        }
    }

});


let p = new Product;
p.price = 9;
p.quantity = 3000;
p.show();

console.log(`${p.price}`*`${p.quantity}`);