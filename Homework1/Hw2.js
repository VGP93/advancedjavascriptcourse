'use strict';

const Product = {
    price: 5,
    quantity: 500,
    show: function () {
        console.log(`${this.price} ${this.quantity} ${this.calculateProfit()}`);
    },
    calculateProfit: function() {
        return this.price * this.quantity;
    }

}

Product.show();
