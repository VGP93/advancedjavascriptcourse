'use strict';

let items = [5, 24, 73, 17, 3, 91];

Array.prototype.findMin = function(){
    //връща минималния елемент в масива
    let min = items[0];
    for(let i = 1; i < items.length; i++) {
        if (min > items[i]) {
            min = items[i];
        }
    }
    console.log(min);    
};

Array.prototype.findMax = function(){
   //връща максималният елемент в масива
   let max = items[0];
   for(let i = 1; i < items.length; i++){
    if(max < items[i]){
        max = items[i];
    }
   }
   console.log(max);    
   
};

items.findMin();
items.findMax();